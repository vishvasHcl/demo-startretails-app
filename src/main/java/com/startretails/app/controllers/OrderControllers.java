package com.startretails.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.startretails.app.dto.OrderRequestDto;
import com.startretails.app.dto.OrderResponseDto;
import com.startretails.app.exception.ResourceNotFoundException;
import com.startretails.app.service.OrderService;

@RequestMapping("/api/order")
@RestController
public class OrderControllers {

	@Autowired
	OrderService orderService;

	@GetMapping("/byuser")
	public ResponseEntity<List<OrderResponseDto>> findByUser(@RequestParam("userId") Integer userId) {
		return ResponseEntity.ok(orderService.findByuserId(userId));
	}

	@PostMapping
	public ResponseEntity<OrderResponseDto> create(@RequestBody(required = true) OrderRequestDto orderRequestDto)
			throws ResourceNotFoundException {
		return new ResponseEntity<>(orderService.placeOrder(orderRequestDto), HttpStatus.CREATED);
	}

}
