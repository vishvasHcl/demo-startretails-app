package com.startretails.app.controllers;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.startretails.app.dto.ProductResponseDto;
import com.startretails.app.exception.ResourceNotFoundException;
import com.startretails.app.service.ProductService;

@RequestMapping("/api/product")
@RestController
public class ProductControllers {

	@Autowired
	ProductService productService;

	@GetMapping("/bynames")
	public ResponseEntity<List<ProductResponseDto>> findProducts(
			@RequestParam(value = "productName", required = false) String productName,
			@RequestParam(value = "categoryName", required = false) String categoryName)
			throws ResourceNotFoundException {

		List<ProductResponseDto> products = null;
		if (StringUtils.isBlank(categoryName) && StringUtils.isBlank(productName)) {
			throw new ResourceNotFoundException("Invalid request. Please provide Product Name or Category Name");
		}
		if (StringUtils.isNotBlank(categoryName)) {
			products = productService.findByCategory(categoryName);
		} else {
			products = productService.findByName(productName);
		}
		return ResponseEntity.ok(products);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ProductResponseDto> findBy(@PathVariable("id") Integer id) throws ResourceNotFoundException {
		return ResponseEntity.ok(productService.findById(id));
	}

}
