package com.startretails.app.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.startretails.app.entities.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
	List<Category> findByNameContains(String name);
}
