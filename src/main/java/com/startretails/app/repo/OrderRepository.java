package com.startretails.app.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.startretails.app.entities.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {
	List<Order> findByUserId(Integer userId);
}
