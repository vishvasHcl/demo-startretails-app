package com.startretails.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.startretails.app.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
