package com.startretails.app.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.startretails.app.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

	List<Product> findByNameContains(String name);

	List<Product> findByCategoryId(Integer categoryId);

}
