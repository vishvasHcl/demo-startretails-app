package com.startretails.app.dto;

public class UserAccountDto {
	private Long accountNum;
	private Double balance;

	public UserAccountDto(Long accountNum, Double balance) {
		super();
		this.accountNum = accountNum;
		this.balance = balance;
	}

	public UserAccountDto() {
		super();
	}

	public Long getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(Long accountNum) {
		this.accountNum = accountNum;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

}
