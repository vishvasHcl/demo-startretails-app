package com.startretails.app.dto;

public class OrderRequestDto {

	private Integer productId;
	private Integer quantity;
	private Long bankAccountNum;
	private Integer userId;

	public OrderRequestDto() {
		super();
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Long getBankAccountNum() {
		return bankAccountNum;
	}

	public void setBankAccountNum(Long bankAccountNum) {
		this.bankAccountNum = bankAccountNum;
	}

}
