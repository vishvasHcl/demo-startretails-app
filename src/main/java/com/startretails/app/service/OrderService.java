package com.startretails.app.service;

import java.util.List;

import com.startretails.app.dto.OrderRequestDto;
import com.startretails.app.dto.OrderResponseDto;
import com.startretails.app.exception.ResourceNotFoundException;

public interface OrderService {

	Long STORE_AC_NUMBER = 2l;
	String ORDER_PAYMENT_REMARKS = "startretails.com - order payment";

	public OrderResponseDto placeOrder(OrderRequestDto orderRequestDto) throws ResourceNotFoundException;

	public List<OrderResponseDto> findByuserId(Integer userId);
}
