package com.startretails.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.startretails.app.dto.ProductResponseDto;
import com.startretails.app.entities.Category;
import com.startretails.app.entities.Product;
import com.startretails.app.exception.ResourceNotFoundException;
import com.startretails.app.repo.CategoryRepository;
import com.startretails.app.repo.ProductRepository;
import com.startretails.app.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;
	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public List<ProductResponseDto> findByName(String name) {
		return convertToResponseDto(productRepository.findByNameContains(name));
	}

	@Override
	public ProductResponseDto findById(Integer id) throws ResourceNotFoundException {
		Product product = productRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Product not found for this id :: " + id));
		ProductResponseDto productResponseDto = new ProductResponseDto();
		BeanUtils.copyProperties(product, productResponseDto);
		return productResponseDto;
	}

	@Override
	public List<ProductResponseDto> findByCategory(String categoryName) {
		List<Product> products = new ArrayList<>();
		List<Category> categories = categoryRepository.findByNameContains(categoryName);
		categories.stream().forEach((category) -> {
			products.addAll(category.getProducts());
		});
		return convertToResponseDto(products);
	}

	private List<ProductResponseDto> convertToResponseDto(List<Product> products) {
		List<ProductResponseDto> productResponseDtos = new ArrayList<>();
		if (!products.isEmpty()) {
			products.stream().forEach(product -> {
				ProductResponseDto productResponseDto = new ProductResponseDto();
				BeanUtils.copyProperties(product, productResponseDto);
				productResponseDtos.add(productResponseDto);
			});
		}
		return productResponseDtos;
	}
}
