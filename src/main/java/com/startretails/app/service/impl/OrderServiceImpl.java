package com.startretails.app.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.startretails.app.dto.OrderRequestDto;
import com.startretails.app.dto.OrderResponseDto;
import com.startretails.app.dto.UserAccountDto;
import com.startretails.app.entities.Order;
import com.startretails.app.entities.Product;
import com.startretails.app.entities.User;
import com.startretails.app.exception.ResourceNotFoundException;
import com.startretails.app.repo.OrderRepository;
import com.startretails.app.repo.ProductRepository;
import com.startretails.app.repo.UserRepository;
import com.startretails.app.service.OrderService;
import com.startretails.app.service.client.AbcBankUserAccountClient;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepository orderRepository;
	@Autowired
	ProductRepository productRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	AbcBankUserAccountClient abcBankUserAccountClient;

	@Override
	@Transactional
	public OrderResponseDto placeOrder(OrderRequestDto orderRequestDto) throws ResourceNotFoundException {
		OrderResponseDto orderResponseDto = new OrderResponseDto();
		Optional<Product> productFromDb = productRepository.findById(orderRequestDto.getProductId());
		Optional<User> userFromDb = userRepository.findById(orderRequestDto.getUserId());
		if (productFromDb.isPresent() && userFromDb.isPresent()) {
			Product product = productFromDb.get();
			Double totalAmount = orderRequestDto.getQuantity() * product.getPrice();
			User user = userFromDb.get();

			ResponseEntity<UserAccountDto> orderPaymentResp = null;
			String orderNumber = "Ord0001" + new Random().nextInt();
			try {
				Long fromAccountNum = null != orderRequestDto.getBankAccountNum() ? orderRequestDto.getBankAccountNum()
						: user.getBankAcNumber();
				orderPaymentResp = abcBankUserAccountClient.orderPayment(fromAccountNum, STORE_AC_NUMBER, totalAmount,
						ORDER_PAYMENT_REMARKS);
				if (orderPaymentResp.getStatusCode() == HttpStatus.OK) {
					Order order = new Order();
					order.setOrderDate(LocalDate.now());
					order.setOrderNumber(orderNumber);
					order.setProductName(product.getName());
					order.setQuantity(orderRequestDto.getQuantity());
					order.setStatus("PLACED");
					order.setTotalAmount(totalAmount);
					order.setUserId(orderRequestDto.getUserId());

					orderRepository.save(order);

					BeanUtils.copyProperties(order, orderResponseDto);
					orderResponseDto
							.setOrderDateStr(order.getOrderDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
				}
			} catch (ResourceNotFoundException e) {
				e.printStackTrace();
				throw new ResourceNotFoundException("Payment not successful for your Order Number :: " + orderNumber);
			}
		}
		return orderResponseDto;
	}

	@Override
	public List<OrderResponseDto> findByuserId(Integer userId) {
		List<OrderResponseDto> orderResponseDtos = new ArrayList<>();
		orderRepository.findByUserId(userId).stream().forEach(order -> {
			OrderResponseDto orderResponseDto = new OrderResponseDto();
			BeanUtils.copyProperties(order, orderResponseDto);
			orderResponseDto.setOrderDateStr(order.getOrderDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			orderResponseDtos.add(orderResponseDto);
		});
		return orderResponseDtos;
	}

}
