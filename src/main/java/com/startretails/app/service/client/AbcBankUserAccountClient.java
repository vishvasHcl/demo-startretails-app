package com.startretails.app.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.startretails.app.dto.UserAccountDto;
import com.startretails.app.exception.ResourceNotFoundException;

//@FeignClient(value = "Abc-Banking-Service", url = "http://localhost:8081/api/account/fundTransfer")
@FeignClient(name = "abcbankapp")
public interface AbcBankUserAccountClient {
	@PostMapping("/api/account/fundTransfer")
	public ResponseEntity<UserAccountDto> orderPayment(@RequestParam(value = "srcAc") Long userAccountNum,
			@RequestParam(value = "desAc") Long storeAccountNum, @RequestParam(value = "amount") Double amount,
			@RequestParam(value = "remarks", required = false) String remarks) throws ResourceNotFoundException;
}
