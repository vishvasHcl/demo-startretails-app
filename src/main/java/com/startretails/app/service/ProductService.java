package com.startretails.app.service;

import java.util.List;

import com.startretails.app.dto.ProductResponseDto;
import com.startretails.app.exception.ResourceNotFoundException;

public interface ProductService {
	public List<ProductResponseDto> findByName(String name);

	public List<ProductResponseDto> findByCategory(String categoryName);

	public ProductResponseDto findById(Integer id) throws ResourceNotFoundException;
}
